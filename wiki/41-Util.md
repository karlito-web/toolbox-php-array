# Karlito Web
#### by Karlito Web

### Description:  
Helpers for PHP [Array](http://brandonwamboldt.github.io/utilphp/).  
Based on http://brandonwamboldt.github.io/utilphp/

***
### Usage:  
``` php
use KarlitoWeb\Toolbox\Tableau;
```

* Flatten a multi-dimensional array into a one dimensional array.
``` php
UtilPHP::flatten(array $array)
```
* Returns an array containing all the elements of arr1 after applying the callback function to each one.
``` php
UtilPHP::clean(array $array)
```
* Returns the first element in an array.
``` php
UtilPHP::first(array $array)
```
* Returns the last element in an array.
``` php
UtilPHP::last(array $array)
```
