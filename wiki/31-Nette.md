# Karlito Web
#### by Karlito Web

### Description:  
Helpers for PHP [Array](hhttps://doc.nette.org/en/3.0/arrays).  
Based on https://doc.nette.org/en/3.0/arrays

***
### Usage:  
``` php
use KarlitoWeb\Toolbox\Tableau;
```

* Transforms multidimensional array to flat array.
``` php
Nette::flatten(array $array)
```

* Inserts the contents of the $inserted array into the $array immediately after the $key. If $key is null (or does not exist), it is inserted at the end.
``` php
Nette::insertAfter(array $datas, string $key, array $insert)
```

* Inserts the contents of the $inserted array into the $array before the $key. If $key is null (or does not exist), it is inserted at the beginning.
``` php
Nette::insertBefore(array $datas, string $key, array $insert)
```
