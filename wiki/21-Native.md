# Karlito Web
#### by Karlito Web

### Description:  
Helpers for PHP [Array](https://www.php.net/manual/fr/ref.array.php)

***
``` php
use KarlitoWeb\Toolbox\Tableau;
```

### Usage:  
* Flatten a multi-dimensional array into a one dimensional array.
``` php
Native::flatten(array $array)
```

* Supprime les valeurs en double d'un tableau
``` php
Native::removeDuplicate(array $array)
```

* Supprime les espaces pour toutes les valeurs d'un tableau
``` php
Native::trimmer(array $array)
```

* Trie un tableau en comparant les éléments comme des chaînes de caractères 
``` php
Native::sort(array $array)
```

* Dédoublonne un tableau en comparant les éléments comme des chaînes, suivant la locale courante
``` php
Native::unique(array $array)
```
