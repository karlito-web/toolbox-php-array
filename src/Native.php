<?php

declare(strict_types=1);

namespace KarlitoWeb\Toolbox\Tableau;

/**
 * @author      Karlito15                               <giancarlo.palumbo@free.fr>
 * @license     https://opensource.org/license/mit/     MIT
 * @link        https://www.php.net/manual/fr/class.recursivearrayiterator.php
 * @package     karlito-web/toolbox-php-array
 * @subpackage  nette/utils
 * @version     3.0.0
 */
class Native
{
    /**
     * Flatten a multi-dimensional array into a one dimensional array.
     *
     * @param array $array
     * @return array
     */
    public static function flatten(array $array): array
    {
        $iterator = new \RecursiveIteratorIterator(new \RecursiveArrayIterator($array));
        $data = [];
        foreach($iterator as $item) {
            $data[] = $item;
        }

        return $data;
    }

    /**
     * Flatten a multi-dimensional array into a one dimensional array.
     *
     * @param array $array
     * @return array<int, mixed>|null
     */
    public static function flattenArrayWalk(array $array): ?array
    {
        array_walk_recursive($array, function ($b) use (&$return) { $return[] = $b; });
        return $return;
    }

    /**
     * Flatten a multi-dimensional array into a one dimensional array.
     *
     * @param array $array
     * @return array
     */
    public static function flattenArrayMerge(array $array): array
    {
        $flat = [];
        foreach($array as $element) {
            if (is_array($element)) {
                $flat = array_merge($flat, self::flattenArrayMerge($element));
            } else {
                $flat[] = $element;
            }
        }
        return $flat;
    }

    /**
     * Supprime les valeurs en double d'un tableau
     *
     * @param array $array
     * @return array
     */
    public static function removeDuplicate(array $array): array
    {
        return array_map("unserialize", array_unique(array_map("serialize", $array)));
    }

    /**
     * Supprime les espaces pour toutes les valeurs d'un tableau
     *
     * @param array $array
     * @return array
     */
    public static function trimmer(array $array): array
    {
        return array_map('trim', $array);
    }

    /**
     * Trie un tableau en comparant les éléments comme des chaînes de caractères
     *
     * @param array $array
     * @return array
     */
    public static function sort(array $array): array
    {
        sort($array, SORT_STRING|SORT_FLAG_CASE);

        return $array;
    }

    /**
     * Dédoublonne un tableau en comparant les éléments comme des chaînes, suivant la locale courante
     *
     * @param array $array
     * @return array
     */
    public static function unique(array $array): array
    {
        return array_unique($array, SORT_LOCALE_STRING);
    }
}
