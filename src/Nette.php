<?php

declare(strict_types=1);

namespace KarlitoWeb\Toolbox\Tableau;

use Nette\Utils\Arrays;

/**
 * @author      Karlito15                               <giancarlo.palumbo@free.fr>
 * @license     https://opensource.org/license/mit/     MIT
 * @link        https://doc.nette.org/en/utils/arrays
 * @package     karlito-web/toolbox-php-array
 * @subpackage  nette/utils
 * @version     3.0.0
 */
class Nette
{
    /**
     * Flatten a multi-dimensional array into a one dimensional array.
     *
     * @param array $array
     * @return array
     */
    public static function flatten(array $array): array
    {
        return Arrays::flatten($array);
    }

    /**
     * Inserts the contents of the $inserted array into the $array immediately after the $key.
     * If $key is null (or does not exist), it is inserted at the end.
     *
     * @param array  $datas
     * @param string $key
     * @param array  $insert
     */
    public static function insertAfter(array $datas, string $key, array $insert): void
    {
        Arrays::insertAfter($datas, $key, $insert);
    }

    /**
     * Inserts the contents of the $inserted array into the $array before the $key.
     * If $key is null (or does not exist), it is inserted at the beginning
     *
     * @param array  $datas
     * @param string $key
     * @param array  $insert
     */
    public static function insertBefore(array $datas, string $key, array $insert): void
    {
        Arrays::insertBefore($datas, $key, $insert);
    }
}
