<?php

declare(strict_types=1);

namespace KarlitoWeb\Toolbox\Tableau;

use utilphp\util as UtilPHP;

/**
 * @author      Karlito15                               <giancarlo.palumbo@free.fr>
 * @license     https://opensource.org/license/mit/     MIT
 * @link        https://brandonwamboldt.github.io/utilphp/#arrays
 * @package     karlito-web/toolbox-php-array
 * @subpackage  brandonwamboldt/utilphp
 * @version     3.0.0
 */
class Util
{
    /**
     * Flatten a multi-dimensional array into a one dimensional array.
     *
     * @param array $array
     * @return array
     */
    public static function flatten(array $array): array
    {
        return UtilPHP::array_flatten($array);
    }

    /**
     * Returns an array containing all the elements of arr1 after applying
     * the callback function to each one.
     *
     * @param array $array
     * @return array
     * @example util::array_clean([ 'a', 'b', '', null, false, 0]); Returns array('a', 'b');
     */
    public static function clean(array $array): array
    {
        return UtilPHP::array_clean($array);
    }

    /**
     * Returns the first element in an array.
     *
     * @param array $array
     * @return mixed
     * @example util::array_first( ['a', 'b', 'c'] ); Returns 'a'
     */
    public static function first(array $array): mixed
    {
        return UtilPHP::array_first($array);
    }

    /**
     * Returns the last element in an array.
     *
     * @param  array $array
     * @return mixed
     * @example util::array_last( ['a', 'b', 'c'] ); Returns 'c'
     */
    public static function last(array $array): mixed
    {
        return UtilPHP::array_last($array);
    }
}
