<?php

use PhpCsFixer\Config;
use PhpCsFixer\Finder;

$rules = [
//    '@PER' => true,
//    '@PER-CS1.0' => true,
//    '@PSR12' => true,
//    '@PSR2' => true,
    '@PhpCsFixer' => true,
    '@Symfony' => true,
];

$finder = Finder::create()->in(__DIR__)
    ->name('src/*.php')
    ->exclude('tests')
    ->ignoreDotFiles(true)
    ->ignoreVCS(true)
;

$config = new Config();

return $config->setFinder($finder)->setRules($rules)->setCacheFile('var/phpcs/.php-cs-fixer.cache')->setRiskyAllowed(true)->setUsingCache(true);
